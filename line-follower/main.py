import io
import time
import threading
import picamera
import yaml
import cv2
import numpy as np

# Create a pool of image processors
done = False
lock = threading.Lock()
pool = []

class ImageProcessor(threading.Thread):
    def __init__(self, options):
        super(ImageProcessor, self).__init__()
        self.stream = io.BytesIO()
        self.event = threading.Event()
        self.terminated = False
        self.options = options
        self.start()

    def run(self):
        # This method runs in a separate thread
        global done
        while not self.terminated:
            # Wait for an image to be written to the stream
            if self.event.wait(1):
                try:
                    self.stream.seek(0)
                    # Read the image and do some processing on it
                    data = np.fromstring(self.stream.getvalue(), dtype=np.uint8)
                    im = cv2.imdecode(data, 1) # BGR
                    # Select a subset of pixels to look at. Assuming we have a fast algorithm and refresh rate, and the camera is pointing relatively close to the water tower it should rarely move outside this area.
                    im = im[self.options.crop_box[0]:self.options.crop_box[2], self.options.crop_box[1]:self.options.crop_box[3]]
                    # Select contours and organise into blocks. Choose closest to centre (this should help prevent gridlock issues)
                    contours, hierarchy = cv2.findContours(im.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                    out = np.zeros_like(im)
                    cv2.drawContours(out, contours, -1, 255, 3)
                    cv2.imwrite('contour.png', out)
                    cv2.imwrite('normal.png', im)
                    done=True
                finally:
                    # Reset the stream and event
                    self.stream.seek(0)
                    self.stream.truncate()
                    self.event.clear()
                    # Return ourselves to the pool
                    with lock:
                        pool.append(self)

def streams():
    while not done:
        with lock:
            if pool:
                processor = pool.pop()
            else:
                processor = None
        if processor:
            yield processor.stream
            processor.event.set()
        else:
            # When the pool is starved, wait a while for it to refill
            time.sleep(0.1)

# parse options from yaml
filename = "config.yaml"
options = {}
with open(filename) as fn:
    options = yaml.load(file.read())

with picamera.PiCamera() as camera:
    pool = [ImageProcessor(options) for i in range(4)]
    camera.resolution = (640, 480)
    camera.framerate = 30
    camera.start_preview()
    time.sleep(2)
    camera.capture_sequence(streams(), use_video_port=True)

# Shut down the processors in an orderly fashion
while pool:
    with lock:
        processor = pool.pop()
    processor.terminated = True
    processor.join()
